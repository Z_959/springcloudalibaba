package com.atguigu.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 17:58
 */
//@Component
public class MyLb implements LoadBalance {
    private AtomicInteger atomicInteger = new AtomicInteger(0);
    // 访问次数
    private final int getAndSwap(){
        int current;
        int next;

        for (;;){
            current = atomicInteger.get();
            next = current > 2000? 0: current + 1;
            if (atomicInteger.compareAndSet(current, next)){
                return next;
            }
        }
    }
    @Override
    public ServiceInstance instances(List<ServiceInstance> serviceInstances) {
        int count = getAndSwap() % serviceInstances.size();
        ServiceInstance serviceInstance = serviceInstances.get(count);
        return serviceInstance;
    }
}

package com.atguigu.rule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 17:38
 */
@Configuration
public class MyRule {
    @Bean
    public IRule roundRobinRule(){
        return new RandomRule();
    }
}

package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author : 959Zjy
 * @date : 2022/4/19 19:36
 */
@SpringBootApplication
@EnableDiscoveryClient
public class MainApplication8004 {
    public static void main(String[] args){
        SpringApplication.run(MainApplication8004.class, args);
    }
}

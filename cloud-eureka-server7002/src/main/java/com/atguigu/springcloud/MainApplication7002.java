package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author : 959Zjy
 * @date : 2022/4/19 18:54
 */
@SpringBootApplication
@EnableEurekaServer
public class MainApplication7002 {
    public static void main(String[] args){
        SpringApplication.run(MainApplication7002.class, args);
    }
}

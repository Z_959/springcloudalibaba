package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author : 959Zjy
 * @date : 2022/4/19 19:52
 */
@SpringBootApplication
@EnableDiscoveryClient
public class MainApplicationZk80 {
    public static void main(String[] args){
        SpringApplication.run(MainApplicationZk80.class, args);
    }
}

package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author : 959Zjy
 * @date : 2022/4/19 18:05
 */
@SpringBootApplication
@EnableEurekaServer
public class MainApplicationEureka7001 {
    public static void main(String[] args){
        SpringApplication.run(MainApplicationEureka7001.class, args);
    }
}

package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author : 959Zjy
 * @date : 2022/4/19 19:01
 */
@SpringBootApplication
@EnableEurekaClient
public class MainApplication8002 {
    public static void main(String[] args){
        SpringApplication.run(MainApplication8002.class, args);
    }
}

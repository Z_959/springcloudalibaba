package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 21:21
 */
@RestController
@Slf4j
public class OrderController {
    @Resource
    private OrderService orderService;

    @GetMapping("/consumer/payment/hystrix/ok/{id}")
    public String ok(@PathVariable("id") Integer id){
        return orderService.ok(id);
    }

    @GetMapping("/consumer/payment/hystrix/timeout/{id}")
    public String timeout(@PathVariable("id") Integer id){
        return orderService.timeout(id);
    }
}

package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 21:19
 */
@SpringBootApplication
@EnableFeignClients
public class MainApplicationFeignHystrix80 {
    public static void main(String[] args){
        SpringApplication.run(MainApplicationFeignHystrix80.class, args);
    }
}

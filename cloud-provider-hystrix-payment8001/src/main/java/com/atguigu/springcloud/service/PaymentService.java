package com.atguigu.springcloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 20:41
 */
@Service
public class PaymentService {
    public String ok(Integer id){
        return "线程池 " + Thread.currentThread().getName()  + " ok " + id;
    }

    // 服务降级
    @HystrixCommand(fallbackMethod = "timeoutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    })//3s以内走正常逻辑,超过3s走兜底
    public String timeout(Integer id){
        try { TimeUnit.SECONDS.sleep(5);} catch (InterruptedException e) {e.printStackTrace();}
        return "线程池 " + Thread.currentThread().getName()  + " timeout " + id;
    }

    // 服务熔断
    public String timeoutHandler(Integer id){
        return "线程池 " + Thread.currentThread().getName()  + " timeoutHandler " + id;
    }
}

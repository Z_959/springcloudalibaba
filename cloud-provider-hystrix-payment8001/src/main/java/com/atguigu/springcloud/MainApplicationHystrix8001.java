package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 20:40
 */
@SpringBootApplication
@EnableEurekaClient
// 回滚
@EnableCircuitBreaker
public class MainApplicationHystrix8001 {
    public static void main(String[] args){
        SpringApplication.run(MainApplicationHystrix8001.class, args);
    }
}

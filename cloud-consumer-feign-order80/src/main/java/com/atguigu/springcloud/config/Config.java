package com.atguigu.springcloud.config;

import feign.Logger;
import org.slf4j.event.EventRecodingLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 18:21
 */
@Configuration
public class Config {
    @Bean
    feign.Logger.Level level(){
        //FULL为详细日志
        return Logger.Level.FULL;
    }
}

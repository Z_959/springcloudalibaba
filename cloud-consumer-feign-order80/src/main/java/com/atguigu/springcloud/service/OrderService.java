package com.atguigu.springcloud.service;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 18:22
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface OrderService {
    // 获取暴露的接口, 通过Eureka获取下面服务地址
    @GetMapping("/payment/get/{id}")
    public CommonResult get(@PathVariable("id") Long id);
}

package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author : 959Zjy
 * @date : 2022/4/20 18:21
 */
@SpringBootApplication
@EnableFeignClients
public class MainApplicationOpenFeign80 {
    public static void main(String[] args){
        SpringApplication.run(MainApplicationOpenFeign80.class, args);
    }
}
